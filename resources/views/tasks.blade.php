@extends('layouts.app')

@section('content')
  @include('common.errors')

  <div class="card mb-3">
    <div class="card-header">New Task</div>
    <div class="card-body">
      <form action="{{ route('create') }}" method="post">
        {{ csrf_field() }} 
        <div class="form-group">
          <label for="task">Task</label>
          <input id="task" class="form-control" placeholder="Enter new task" type="text" name="name" autofocus>
        </div>
        <button type="submit" class="btn btn-info">Add Task</button>
      </form>
    </div>
  </div>

  <div class="card">
    <div class="card-header">Current Tasks</div>
    <div class="card-body">
      <ul class="list-group list-group-flush">
        @forelse($tasks as $task)
          <li class="list-group-item">
            {{ $task->name }}
            <form action="{{ route('delete', ['task' => $task->id]) }}" method="post" class="mt-1">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
            </form>
          </li>
        @empty
          <li class="list-group-item">Task List is Empty!</li>
        @endforelse
      </ul>
    </div>
  </div>
@endsection
