@if (count($errors) > 0)
  <div class="alert alert-success alert-dismissable fade show mb-3">
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times</span>
    </button>
    <h4 class="alert-heading">Whoops! Something went wrong!</h4>
    <hr>
    <ul class="list-group">
      @foreach ($errors->all() as $error)
      <li class="list-group-item list-group-item-success">{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

