<?php


use App\Task;
use Illuminate\Http\Request;


Route::get('/', function () {
    return view('tasks') 
        ->with('tasks', Task::orderBy('created_at', 'desc')->get());
})->name('index');

Route::post('/task', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect()
            ->route('index')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new Task;
    $task->name = $request->name;
    $task->save();

    return redirect()->route('index');
})->name('create');

Route::delete('/task/{task}', function (Task $task) {
    $task->delete();
    
    return redirect()->route('index');
})->name('delete');
